using System.Net;
using Microsoft.Extensions.Primitives;

namespace Service.Test;

public class HostInfo
{
    public string Host { get; set; }
    public int? Port { get; set; }
    public Dictionary<string, StringValues> Headers { get; set; }
    public string? IpAddress { get; set; }
}