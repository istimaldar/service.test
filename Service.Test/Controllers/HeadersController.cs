using Microsoft.AspNetCore.Mvc;

namespace Service.Test.Controllers;

[ApiController]
[Route("/api/v{version:apiVersion}/[controller]")]
[ApiVersion("1.0")]
public class HeadersController : ControllerBase
{
    private readonly ILogger<HeadersController> _logger;

    public HeadersController(ILogger<HeadersController> logger)
    {
        _logger = logger;
    }

    [HttpGet(Name = "GetWeatherForecast")]
    public HostInfo Get()
    {
        return new HostInfo
        {
            Host = HttpContext.Request.Host.Value,
            Port = HttpContext.Request.Host.Port,
            Headers = HttpContext.Request.Headers.ToDictionary(i => i.Key, i => i.Value),
            IpAddress = HttpContext.Connection.RemoteIpAddress?.ToString()
        };
    }
}